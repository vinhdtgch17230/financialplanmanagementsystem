﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.PlanManagement
{
    public class PlanManagementViewModel
    {
        public int Id { get; set; }
        public string EmployeeName { get; set; }
        public string Title { get; set; }
        public DateTime SubmittedDay { get; set; }
        public int Status { get; set; }
        public string DepartmentName { get; set; }
        public string StatusName { get; set; }
    }
}
