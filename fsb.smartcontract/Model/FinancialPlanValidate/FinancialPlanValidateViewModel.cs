﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.FinancialPlanValidate
{
    public class FinancialPlanValidateViewModel
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public string Units { get; set; }
        public string Currency { get; set; }
        public decimal RealValue { get; set; }
        public decimal ConversionValue { get; set; }
        public string ExpenditureType { get; set; }
        public string Error { get; set; }
    }
}
