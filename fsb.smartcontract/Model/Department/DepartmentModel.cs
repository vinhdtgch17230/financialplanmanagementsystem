﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Department
{
    public class DepartmentModel : ModelBase
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
    }
}
