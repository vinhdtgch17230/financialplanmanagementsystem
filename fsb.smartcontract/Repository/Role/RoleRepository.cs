﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Requests;
using Model.Response;
using Model.Role;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Role
{
    public class RoleRepository : Repository<RoleRepository>, IRoleRepository
    {
        private readonly IConfiguration _configuration;
        public RoleRepository(
            ILogger<RoleRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }

        public Response<UserViewModel> GetRoleByUserId(Guid userId)
        {
            Response<UserViewModel> res = new Response<UserViewModel>();
            try
            {
                var user = _context.Users.Where(x => x.Id == userId && x.IsDelete == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tìm thấy người dùng!";
                    return res;
                }
                else
                {
                    var userRole = _context.UserRoles.FirstOrDefault(x => x.UserId == user.Id);
                    var userRoleName = "";
                    var userRoleNormalizedName = "";
                    if (userRole != null)
                    {
                        var dataUserRole = _context.Roles.FirstOrDefault(x => x.Id == userRole.RoleId);
                        userRoleName = dataUserRole.Name;
                        userRoleNormalizedName = dataUserRole.NormalizedName;
                    }

                    res.Code = StatusCodes.Status200OK;
                    res.Data = new UserViewModel()
                    {
                        Id = user.Id,
                        IsAdmin = user.IsAdmin,
                        UserRoleName = userRoleName,
                        UserRoleNormalizedName = userRoleNormalizedName
                    };
                    return res;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin người dùng!";
                return res;
            }
        }

        public TableResponse<RoleViewModel> GetListRole(SearchRoleModel search)
        {
            TableResponse<RoleViewModel> result = new TableResponse<RoleViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.Roles.Where(x => x.IsDelete == false).Select(x => new RoleViewModel
                {
                    Id = x.Id,
                    RoleName = x.Name,
                    IsActive = x.IsActive
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.RoleName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách nhóm quyền!";
            }
            return result;
        }

        public Response<string> CreateRole(RoleModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.RoleName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên nhóm quyền không được bỏ trống!";
                    return res;
                }

                var data = _context.Roles.FirstOrDefault(x => x.Name == model.RoleName.TrimEnd().TrimStart());
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Nhóm quyền đã tồn tại!";
                    return res;
                }

                Entity.Entity.Role role = new Entity.Entity.Role();
                role.Name = model.RoleName;
                role.NormalizedName = model.RoleName;
                role.IsActive = true;
                _context.Roles.Add(role);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm nhóm quyền thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm nhóm quyền!";
            }

            return res;
        }

        public Response<string> DeleteRole(RoleModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                var role = _context.Roles.FirstOrDefault(x => x.Id == model.Id && x.IsDelete == false);
                if (role == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại nhóm quyền, không thể xóa!";
                    return res;
                }
                role.IsDelete = true;
                role.IsActive = false;
                _context.Roles.Update(role);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa nhóm quyền thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi xóa nhóm quyền!";
            }

            return res;
        }

        public Response<RoleModel> GetRoleById(RoleModel model)
        {
            Response<RoleModel> res = new Response<RoleModel>();

            try
            {
                var query = (from a in _context.Roles
                             where a.IsDelete == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new RoleModel
                {
                    Id = group.Key,
                    RoleName = group.ToList().FirstOrDefault().a.Name
                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin nhóm quyền!";
            }

            return res;
        }

        public Response<string> UpdateRole(RoleModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.RoleName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên nhóm quyền không được bỏ trống!";
                    return res;
                }

                var role = _context.Roles.FirstOrDefault(x => x.IsDelete == false && x.Id == model.Id);
                if (role == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại nhóm quyền, không thể cập nhật!";
                    return res;
                }

                role.NormalizedName = model.RoleName;
                role.Name = model.RoleName;
                _context.Roles.Update(role);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Sửa nhóm quyền thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi sửa nhóm quyền!";
            }

            return res;
        }
    }
}
