﻿using Model.FinancialPlan;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.FinancialPlan
{
    public interface IFinancialPlanRepository
    {
        TableResponse<FinancialPlanViewModel> GetListFinancialPlan(SearchFinancialPlanModel search);
        Response<FinancialPlanModel> GetFinancialPlanById(FinancialPlanModel model);
        List<FinancialPlanModel> CreateFinancialPlan(List<FinancialPlanModel> list);
        //Response<string> ImportExcelFinancialPlanAsync(List<FinancialPlanModel> list);
    }
}
