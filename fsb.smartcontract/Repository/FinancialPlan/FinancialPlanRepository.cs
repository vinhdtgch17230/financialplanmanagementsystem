﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.FinancialPlan;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.FinancialPlan
{
    public class FinancialPlanRepository : Repository<FinancialPlanRepository>, IFinancialPlanRepository
    {
        //Get list, ID,
        //Delete all data in table
        private readonly IConfiguration _configuration;
        public FinancialPlanRepository(
            ILogger<FinancialPlanRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }
        public TableResponse<FinancialPlanViewModel> GetListFinancialPlan(SearchFinancialPlanModel search)
        {
            TableResponse<FinancialPlanViewModel> result = new TableResponse<FinancialPlanViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.FinancialPlans.Where(x => x.IsDelete == false).Select(x => new FinancialPlanViewModel
                {
                    Id = x.Id,
                    ItemName = x.ItemName,
                    Units = x.Units,
                    Currency = x.Currency,
                    RealValue = x.RealValue,
                    ConversionValue = x.ConversionValue,
                    ExpenditureType = x.ExpenditureType,
                    //PlanManagementId = x.PlanManagementId
                }).ToList();


                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.ItemName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                foreach (var item in data)
                {

                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách quản lý Kế hoạch!";
            }
            return result;
        }
        public Response<FinancialPlanModel> GetFinancialPlanById(FinancialPlanModel model)
        {
            Response<FinancialPlanModel> res = new Response<FinancialPlanModel>();

            try
            {
                var query = (from a in _context.FinancialPlans
                             where a.IsDelete == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new FinancialPlanModel
                {
                    Id = group.Key,
                    ItemName = group.ToList().FirstOrDefault().a.ItemName,
                    //PlanManagementId = group.ToList().FirstOrDefault().a.PlanManagementId,
                    Units = group.ToList().FirstOrDefault().a.Units,
                    Currency = group.ToList().FirstOrDefault().a.Currency,
                    RealValue = group.ToList().FirstOrDefault().a.RealValue,
                    ConversionValue = group.ToList().FirstOrDefault().a.ConversionValue,
                    ExpenditureType = group.ToList().FirstOrDefault().a.ExpenditureType

                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy kế hoạch tài chính!";
            }

            return res;
        }
        public List<FinancialPlanModel> CreateFinancialPlan(List<FinancialPlanModel> list)
        {
            List<FinancialPlanModel> res = new List<FinancialPlanModel>();

            try
            {
                foreach (var a in list)
                {
                    Entity.Entity.FinancialPlan financialPlan = new Entity.Entity.FinancialPlan();
                    financialPlan.ItemName = a.ItemName;
                    financialPlan.Units = a.Units;
                    financialPlan.Currency = a.Currency;
                    financialPlan.RealValue = a.RealValue;
                    financialPlan.ConversionValue = a.ConversionValue;
                    financialPlan.ExpenditureType = a.ExpenditureType;


                    _context.FinancialPlans.Add(financialPlan);
                    _context.SaveChanges();
                }



                //res.Code = StatusCodes.Status200OK;
                //res.Message = "Thêm kế hoạch tài chính thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                //res.Code = StatusCodes.Status500InternalServerError;
                //res.Message = "Xảy ra lỗi khi thêm tên kế hoạch tài chính!";
            }

            return res;
        }
        //public Response<string> ImportExcelFinancialPlanAsync(List<FinancialPlanModel> list)
        //{
        //    Response<string> res = new Response<string>();
        //    try
        //    {


        //        //xóa dữ liệu cũ



        //        List<FinancialPlanModel> FinancialPlanModel = new List<FinancialPlanModel>();
        //        Entity.Entity.FinancialPlan financialPlan = new Entity.Entity.FinancialPlan();
        //        foreach (var item in list)
        //        {




        //            //var departmentIds = departmentPlanning.DepartmentIds.Split(",").ToList();
        //            //var unitCodeList = _context.Units.Where(x => x.DepartmentId == departmentId).ToArray();
        //            //int posUnitCode = Array.IndexOf(unitCodeList, departmentPlanningTempUnitCode);
        //            //if (posUnitCode < 0)
        //            //{
        //            //    item.Errors.Add("Bộ phận không thuộc khối trong Import dữ liệu");
        //            //}




        //            var FinancialPlans = new FinancialPlanModel()
        //            {
        //                ItemName = item.ItemName,
        //                Units = item.Units,
        //                Currency = item.Currency,
        //                RealValue = item.RealValue,
        //                ConversionValue = item.ConversionValue,
        //                ExpenditureType = item.ExpenditureType,

        //            };
        //            //importDepartmentPlanningDetails.Add(departmentPlanningDetailImportModel);
        //        }
        //        _context.FinancialPlans.Add(financialPlan);
        //        _context.SaveChanges();
        //        res.Message = "Đọc dữ liệu thành công!";
        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "");
        //        res.Code = Code404;
        //        res.Message = "Xảy ra lỗi khi đọc file excel!";
        //    }
        //    return res;
        //}



    }
}


//private Response<List<DepartmentPlanningDetailImportModel>> ReadExcel(byte[] content)
//{
//    Response<List<DepartmentPlanningDetailImportModel>> res = new Response<List<DepartmentPlanningDetailImportModel>>();
//    try
//    {
//        MemoryStream stream = new MemoryStream(content);
//        using (var reader = ExcelReaderFactory.CreateReader(stream))
//        {
//            var result = reader.AsDataSet();
//            var table = result.Tables[0];
//            int startRow = 2;
//            int maxRow = 1048576 - startRow;
//            List<DepartmentPlanningDetailImportModel> list = new List<DepartmentPlanningDetailImportModel>();
//            for (int i = startRow; i < table.Rows.Count; i++)
//            {
//                var DatePlan = table.Rows[i][0].ToString();
//                var CompanyCode = table.Rows[i][1].ToString();
//                var DepartmentName = table.Rows[i][2].ToString();
//                var UnitName = table.Rows[i][3].ToString();
//                var UnitCode = table.Rows[i][4].ToString();
//                var CostTypeName = table.Rows[i][5].ToString();
//                var ProjectName = table.Rows[i][6].ToString();
//                var CostName = table.Rows[i][7].ToString();
//                var CostCode = table.Rows[i][8].ToString();
//                var ContractStatusName = table.Rows[i][9].ToString();
//                var DecisionNumber = table.Rows[i][10].ToString();
//                var SupplierTypeName = table.Rows[i][11].ToString();
//                var SupplierName = table.Rows[i][12].ToString();
//                var SupplierCode = table.Rows[i][13].ToString();
//                var NaturalCurrency = table.Rows[i][14].ToString();
//                var ExchangeRate = table.Rows[i][15].ToString();
//                var MoneyWeekOne = table.Rows[i][16].ToString();
//                var MoneyWeekTwo = table.Rows[i][17].ToString();
//                var MoneyWeekThree = table.Rows[i][18].ToString();
//                var MoneyWeekFour = table.Rows[i][19].ToString();
//                var MonthOne = table.Rows[i][20].ToString();
//                var MonthTwo = table.Rows[i][21].ToString();
//                var MonthThree = table.Rows[i][22].ToString();
//                var MonthFour = table.Rows[i][23].ToString();
//                var MonthFive = table.Rows[i][24].ToString();
//                var Note = table.Rows[i][25].ToString();

//                var item = new DepartmentPlanningDetailImportModel();
//                item.DatePlan = DatePlan;
//                item.CompanyCode = (!string.IsNullOrEmpty(CompanyCode) ? CompanyCode.Trim() : null);
//                item.DepartmentName = (!string.IsNullOrEmpty(DepartmentName) ? DepartmentName.Trim() : null);
//                item.UnitCode = (!string.IsNullOrEmpty(UnitCode) ? UnitCode.Trim() : null);
//                item.UnitName = (!string.IsNullOrEmpty(UnitName) ? UnitName.Trim() : null);
//                item.CostTypeName = (!string.IsNullOrEmpty(CostTypeName) ? CostTypeName.Trim() : null);
//                item.ProjectName = (!string.IsNullOrEmpty(ProjectName) ? ProjectName.Trim() : null);
//                item.CostName = (!string.IsNullOrEmpty(CostName) ? CostName.Trim() : null);
//                item.CostCode = (!string.IsNullOrEmpty(CostCode) ? CostCode.Trim() : null);
//                item.ContractStatusName = (!string.IsNullOrEmpty(ContractStatusName) ? ContractStatusName.Trim() : null);
//                item.DecisionNumber = (!string.IsNullOrEmpty(DecisionNumber) ? DecisionNumber.Trim() : null);
//                item.SupplierTypeName = (!string.IsNullOrEmpty(SupplierTypeName) ? SupplierTypeName.Trim() : null);
//                item.SupplierName = (!string.IsNullOrEmpty(SupplierName) ? SupplierName.Trim() : null);
//                item.SupplierCode = (!string.IsNullOrEmpty(SupplierCode) ? SupplierCode.Trim() : null);
//                item.NaturalCurrency = (!string.IsNullOrEmpty(NaturalCurrency) ? NaturalCurrency.Trim() : null);
//                item.ExchangeRate = (!string.IsNullOrEmpty(ExchangeRate) ? decimal.Parse(ExchangeRate) : 0);
//                item.MoneyWeekOne = (!string.IsNullOrEmpty(MoneyWeekOne) ? decimal.Parse(MoneyWeekOne) : 0);
//                item.MoneyWeekTwo = (!string.IsNullOrEmpty(MoneyWeekTwo) ? decimal.Parse(MoneyWeekTwo) : 0);
//                item.MoneyWeekThree = (!string.IsNullOrEmpty(MoneyWeekThree) ? decimal.Parse(MoneyWeekThree) : 0);
//                item.MoneyWeekFour = (!string.IsNullOrEmpty(MoneyWeekFour) ? decimal.Parse(MoneyWeekFour) : 0);
//                item.MonthOne = (!string.IsNullOrEmpty(MonthOne) ? decimal.Parse(MonthOne) : 0);
//                item.MonthTwo = (!string.IsNullOrEmpty(MonthTwo) ? decimal.Parse(MonthTwo) : 0);
//                item.MonthThree = (!string.IsNullOrEmpty(MonthThree) ? decimal.Parse(MonthThree) : 0);
//                item.MonthFour = (!string.IsNullOrEmpty(MonthFour) ? decimal.Parse(MonthFour) : 0);
//                item.MonthFive = (!string.IsNullOrEmpty(MonthFive) ? decimal.Parse(MonthFive) : 0);
//                item.Note = (!string.IsNullOrEmpty(Note) ? Note.Trim() : null);
//                list.Add(item);

//            }
//            res.Data = list;
//        }
//    }
//    catch (Exception ex)
//    {
//        _logger.LogError(ex, "");
//        res.Code = StatusCodes.Status500InternalServerError;
//        res.Message = "Xảy ra lỗi khi đọc file!";
//    }
//    return res;
//}