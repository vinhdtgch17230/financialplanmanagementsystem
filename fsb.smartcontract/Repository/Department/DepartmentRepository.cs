﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Department;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Department
{
    public class DepartmentRepository : Repository<DepartmentRepository>, IDepartmentRepository
    {
        private readonly IConfiguration _configuration;
        public DepartmentRepository(
            ILogger<DepartmentRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }
        public TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search)
        {
            TableResponse<DepartmentViewModel> result = new TableResponse<DepartmentViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.Departments.Where(x => x.IsDelete == false).Select(x => new DepartmentViewModel
                {
                    Id = x.Id,
                    DepartmentName = x.DepartmentName,
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.DepartmentName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK; //Mã lỗi
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách Phòng ban!";
            }
            return result;
        }

        public Response<string> CreateDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.DepartmentName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên phòng ban không được bỏ trống!";
                    return res;
                }

                //if (string.IsNullOrEmpty(model.DepartmentCode))
                //{
                //    res.Code = StatusCodes.Status400BadRequest;
                //    res.Message = "Mã phòng ban không được bỏ trống!";
                //    return res;
                //}

                //if (model.CompanyId <= 0)
                //{
                //    res.Code = StatusCodes.Status400BadRequest;
                //    res.Message = "Công ty không được bỏ trống!";
                //    return res;
                //}

                var data = _context.Departments.FirstOrDefault(x => x.DepartmentName == model.DepartmentName.Trim() && x.IsDelete == false);
                if (data != null)
                {
                    res.Code = StatusCodes.Status404NotFound; 
                    res.Message = "Phòng ban đã tồn tại!";
                    return res;
                }

                Entity.Entity.Department department = new Entity.Entity.Department();
                department.DepartmentName = model.DepartmentName;



                _context.Departments.Add(department);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm phòng ban thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm tên phòng ban!";
            }

            return res;
        }

        public Response<string> DeleteDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                var department = _context.Departments.FirstOrDefault(x => x.Id == model.Id && x.IsDelete == false);
                if (department == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại phòng ban, không thể xóa!";
                    return res;
                }
                department.IsDelete = true;

                _context.Departments.Update(department);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xóa phòng ban thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi xóa phòng ban!";
            }

            return res;
        }

        public Response<DepartmentModel> GetDepartmentById(DepartmentModel model)
        {
            Response<DepartmentModel> res = new Response<DepartmentModel>();

            try
            {
                var query = (from a in _context.Departments
                             where a.IsDelete == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new DepartmentModel
                {
                    Id = group.Key,
                    DepartmentName = group.ToList().FirstOrDefault().a.DepartmentName
                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin phòng ban!";
            }

            return res;
        }

        public Response<string> UpdateDepartment(DepartmentModel model)
        {
            Response<string> res = new Response<string>();

            try
            {

                //if (model.CompanyId <= 0)
                //{
                //    res.Code = StatusCodes.Status400BadRequest;
                //    res.Message = "Công ty không được bỏ trống!";
                //    return res;
                //}

                //if (string.IsNullOrEmpty(model.DepartmentCode))
                //{
                //    res.Code = StatusCodes.Status400BadRequest;
                //    res.Message = "Mã phòng ban không được bỏ trống!";
                //    return res;
                //}

                if (string.IsNullOrEmpty(model.DepartmentName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên phòng ban không được bỏ trống!";
                    return res;
                }

                var department = _context.Departments.FirstOrDefault(x => x.IsDelete == false && x.Id == model.Id);
                if (department == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại phòng ban, không thể cập nhật!";
                    return res;
                }

                department.DepartmentName = model.DepartmentName;

                _context.Departments.Update(department);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Sửa phòng ban thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi sửa phòng ban!";
            }

            return res;
        }

        public List<SelectListItem> GetListDepartmentForCombo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var _data = _context.Departments.Where(x => x.IsDelete == false)
                    .Select(department => new SelectListItem
                    {
                        Value = department.Id.ToString(),
                        Text = department.DepartmentName
                    }).ToList();
                list = _data;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi lấy list");
            }
            return list;
        }
    }
}
