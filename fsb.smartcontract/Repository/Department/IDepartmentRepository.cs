﻿using Model.Department;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Department
{
    public interface IDepartmentRepository
    {
        TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search);
        Response<string> CreateDepartment(DepartmentModel model);
        Response<string> DeleteDepartment(DepartmentModel model);
        Response<DepartmentModel> GetDepartmentById(DepartmentModel model);
        Response<string> UpdateDepartment(DepartmentModel model);
    }
}


