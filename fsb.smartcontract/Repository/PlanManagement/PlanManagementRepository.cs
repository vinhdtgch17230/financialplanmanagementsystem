﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.FinancialPlan;
using Model.PlanManagement;
using Model.Requests;
using Model.Response;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.PlanManagement
{
    public class PlanManagementRepository : Repository<PlanManagementRepository>, IPlanManagementRepository
    {
        private readonly IConfiguration _configuration;
        public PlanManagementRepository(
            ILogger<PlanManagementRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }
        public TableResponse<PlanManagementViewModel> GetListPlanManagement(SearchPlanManagementModel search)
        {
            TableResponse<PlanManagementViewModel> result = new TableResponse<PlanManagementViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.PlanManagements.Where(x => x.IsDelete == false).Select(x => new PlanManagementViewModel
                {
                    Id = x.Id,
                    EmployeeName = x.EmployeeName,
                    Title = x.Title,
                    SubmittedDay = x.SubmittedDay,
                    Status = x.Status,
                    DepartmentName = x.Department.DepartmentName
                }).ToList();


                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.EmployeeName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                foreach (var item in data)
                {


                    if (item.Status == 1)
                    {
                        item.StatusName = "Submitted";
                    }
                    else if (item.Status == 2)
                    {
                        item.StatusName = "Approve";
                    }
                    else
                    {
                        item.StatusName = "Reject";
                    }
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách quản lý Kế hoạch!";
            }
            return result;
        }

        public Response<string> CreatePlanManagement(PlanManagementModel model)
        {
            Response<string> res = new Response<string>();

            try
            {
                if (string.IsNullOrEmpty(model.EmployeeName))
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên nhân viên không được bỏ trống!";
                    return res;
                }

                if (model.DepartmentId <= 0)
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tên phòng ban không được bỏ trống!";
                    return res;
                }

                if (model.Title == null)
                {
                    res.Code = StatusCodes.Status400BadRequest;
                    res.Message = "Tiêu đề không được bỏ trống!";
                    return res;
                }

                Entity.Entity.PlanManagement planManagement = new Entity.Entity.PlanManagement();
                planManagement.EmployeeName = model.EmployeeName;
                planManagement.DepartmentId = model.DepartmentId;
                planManagement.Title = model.Title;
                planManagement.SubmittedDay = model.SubmittedDay;
                planManagement.Status = 1;

                _context.PlanManagements.Add(planManagement);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Thêm kế hoạch quản lý thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi thêm kế hoạch quản lý!";
            }

            return res;
        }
        public Response<PlanManagementModel> GetPlanManagementById(PlanManagementModel model)
        {
            Response<PlanManagementModel> res = new Response<PlanManagementModel>();

            try
            {
                var query = (from a in _context.PlanManagements
                             where a.IsDelete == false && a.Id == model.Id
                             select new
                             {
                                 a
                             }).ToList();

                var group = query.GroupBy(x => x.a.Id).Select(group => new PlanManagementModel
                {
                    Id = group.Key,
                    EmployeeName = group.ToList().FirstOrDefault().a.EmployeeName,
                    DepartmentId = group.ToList().FirstOrDefault().a.DepartmentId,
                    Title = group.ToList().FirstOrDefault().a.Title,
                    SubmittedDay = group.ToList().FirstOrDefault().a.SubmittedDay,
                    Status = group.ToList().FirstOrDefault().a.Status

                }).ToList();

                res.Code = StatusCodes.Status200OK;
                res.Data = group.FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status400BadRequest;
                res.Message = "Xảy ra lỗi khi lấy thông tin quản lý kế hoạch!";
            }

            return res;
        }
        //public Response<List<Ex>> Import(Ex model)
        //{
        //    Response<string> res = new Response<string>();

        //    //try
        //    //{
        //    //    if (string.IsNullOrEmpty(model.CompanyName))
        //    //    {
        //    //        res.Code = StatusCodes.Status400BadRequest;
        //    //        res.Message = "Tên công ty không được bỏ trống!";
        //    //        return res;
        //    //    }

        //    //    if (string.IsNullOrEmpty(model.CompanyCode))
        //    //    {
        //    //        res.Code = StatusCodes.Status400BadRequest;
        //    //        res.Message = "Mã công ty không được bỏ trống!";
        //    //        return res;
        //    //    }

        //    //    var data = _context.Companies.FirstOrDefault(x => x.CompanyName == model.CompanyName.TrimEnd().TrimStart());
        //    //    if (data != null)
        //    //    {
        //    //        res.Code = StatusCodes.Status404NotFound;
        //    //        res.Message = "Công ty đã tồn tại!";
        //    //        return res;
        //    //    }

        //    //    Entity.Entity.Company company = new Entity.Entity.Company();
        //    //    company.CompanyName = model.CompanyName;
        //    //    company.CompanyCode = model.CompanyCode;


        //    //    _context.Companies.Add(company);
        //    //    _context.SaveChanges();

        //    //    res.Code = StatusCodes.Status200OK;
        //    //    res.Message = "Thêm công ty thành công!";
        //    //    return res;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    _logger.LogError(ex, "");
        //    //    res.Code = StatusCodes.Status500InternalServerError;
        //    //    res.Message = "Xảy ra lỗi khi thêm tên công ty!";
        //    //}

        //    //return res;
        //}
        public Response<string> DeletePlanManagement(PlanManagementModel model)
        {
            Response<string> res = new Response<string>();

            try
            {

                var planManagement = _context.PlanManagements.FirstOrDefault(x => x.IsDelete == false && x.Id == model.Id);
                if (planManagement == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không tồn tại kế hoạch quản lý, không thể cập nhật!";
                    return res;
                }

                if (planManagement.Status > 1)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Không thể xoá thông tin vì đã thay đổi trạng thái";
                    return res;
                }

                planManagement.IsDelete = true;
                _context.PlanManagements.Update(planManagement);
                _context.SaveChanges();

                res.Code = StatusCodes.Status200OK;
                res.Message = "Xoá trạng thái thành công!";
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Xảy ra lỗi khi cập nhật trạng thái!";
            }

            return res;
        }



        //public async Task<bool> Handle(ImportExcelFacilityHeadcountCommand request, CancellationToken cancellationToken)
        //{
        //    foreach (var r in request.importFacilityHeadcountModels)
        //    {
        //        if (r.Name == null || r.StaffId == null || r.Company == null)
        //        {
        //            throw new ArgumentException(Constant.ErrorFromServer + "Import Facility headcount error. Name, StaffId, Company is not null");
        //        }


        //        int checkCompany = 0;
        //        var checkCompanyName = "";
        //        if (r.Company.ToLower() == "petronas")
        //        {
        //            checkCompany = 0;
        //        }
        //        else if (r.Company.ToLower() == "contractor")
        //        {
        //            checkCompany = 1;
        //            checkCompanyName = r.CompanyName;
        //        }
        //        else if (r.Company.ToLower() == "others")
        //        {
        //            checkCompany = 2;
        //            checkCompanyName = r.CompanyName;
        //        }
        //        else
        //        {
        //            throw new ArgumentException(Constant.ErrorFromServer + "Import Facility headcount error.");
        //        }

        //        var item = new FacilityHeadcount()
        //        {
        //            Name = r.Name,
        //            StaffId = r.StaffId,
        //            Company = checkCompany,
        //            CompanyName = checkCompanyName,
        //            FacilityId = request.FacilityId,
        //            IsIntegrate = false
        //        };

        //        UnitOfWork.FacilityHeadcountRepository.Add(item);

        //    }
        //    return await UnitOfWork.SaveChangesAsync() > 0;
        //}
    }
}
