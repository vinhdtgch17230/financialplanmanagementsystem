﻿using Microsoft.AspNetCore.Http;
using Model.FinancialPlan;
using Model.PlanManagement;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Repository.PlanManagement
{
    public interface IPlanManagementRepository
    {
        TableResponse<PlanManagementViewModel> GetListPlanManagement(SearchPlanManagementModel search);
        Response<string> CreatePlanManagement(PlanManagementModel model);
        Response<PlanManagementModel> GetPlanManagementById(PlanManagementModel model);
        //Response<List<Ex>> Import(Ex model);
        Response<string> DeletePlanManagement(PlanManagementModel model);
    }
}
