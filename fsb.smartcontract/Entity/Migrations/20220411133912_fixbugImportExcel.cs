﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class fixbugImportExcel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(8338),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7734));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(7942),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7010));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(8705),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 547, DateTimeKind.Local).AddTicks(167));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(7573),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 546, DateTimeKind.Local).AddTicks(8236));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7734),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(8338));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7010),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(7942));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 547, DateTimeKind.Local).AddTicks(167),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(8705));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 546, DateTimeKind.Local).AddTicks(8236),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(7573));
        }
    }
}
