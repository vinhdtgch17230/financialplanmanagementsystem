﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class Please : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinancialPlan_PlanManagement_PlanManagementId",
                table: "FinancialPlan");

            migrationBuilder.DropIndex(
                name: "IX_FinancialPlan_PlanManagementId",
                table: "FinancialPlan");

            migrationBuilder.DropColumn(
                name: "PlanManagementId",
                table: "FinancialPlan");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 578, DateTimeKind.Local).AddTicks(495),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 284, DateTimeKind.Local).AddTicks(6764));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 578, DateTimeKind.Local).AddTicks(60),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 284, DateTimeKind.Local).AddTicks(6166));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 574, DateTimeKind.Local).AddTicks(9912),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 281, DateTimeKind.Local).AddTicks(8339));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 574, DateTimeKind.Local).AddTicks(8836),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 281, DateTimeKind.Local).AddTicks(7384));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 284, DateTimeKind.Local).AddTicks(6764),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 578, DateTimeKind.Local).AddTicks(495));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 284, DateTimeKind.Local).AddTicks(6166),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 578, DateTimeKind.Local).AddTicks(60));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 281, DateTimeKind.Local).AddTicks(8339),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 574, DateTimeKind.Local).AddTicks(9912));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 24, 34, 281, DateTimeKind.Local).AddTicks(7384),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 11, 55, 14, 574, DateTimeKind.Local).AddTicks(8836));

            migrationBuilder.AddColumn<int>(
                name: "PlanManagementId",
                table: "FinancialPlan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_FinancialPlan_PlanManagementId",
                table: "FinancialPlan",
                column: "PlanManagementId");

            migrationBuilder.AddForeignKey(
                name: "FK_FinancialPlan_PlanManagement_PlanManagementId",
                table: "FinancialPlan",
                column: "PlanManagementId",
                principalTable: "PlanManagement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
