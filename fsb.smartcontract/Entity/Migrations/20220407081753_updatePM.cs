﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class updatePM : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Titile",
                table: "PlanManagement",
                newName: "Title");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7734),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 366, DateTimeKind.Local).AddTicks(5347));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7010),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 366, DateTimeKind.Local).AddTicks(4977));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 547, DateTimeKind.Local).AddTicks(167),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 364, DateTimeKind.Local).AddTicks(7593));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 546, DateTimeKind.Local).AddTicks(8236),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 364, DateTimeKind.Local).AddTicks(6684));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Title",
                table: "PlanManagement",
                newName: "Titile");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 366, DateTimeKind.Local).AddTicks(5347),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7734));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 366, DateTimeKind.Local).AddTicks(4977),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 549, DateTimeKind.Local).AddTicks(7010));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 364, DateTimeKind.Local).AddTicks(7593),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 547, DateTimeKind.Local).AddTicks(167));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 29, 25, 364, DateTimeKind.Local).AddTicks(6684),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 15, 17, 52, 546, DateTimeKind.Local).AddTicks(8236));
        }
    }
}
