﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class updatetblFinancialplan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "FinancialPlan");

            migrationBuilder.RenameColumn(
                name: "status",
                table: "PlanManagement",
                newName: "Status");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(6271),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 153, DateTimeKind.Local).AddTicks(9515));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(5825),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 153, DateTimeKind.Local).AddTicks(9077));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(8696),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 150, DateTimeKind.Local).AddTicks(8075));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(7727),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 150, DateTimeKind.Local).AddTicks(6816));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Status",
                table: "PlanManagement",
                newName: "status");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 153, DateTimeKind.Local).AddTicks(9515),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(6271));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 153, DateTimeKind.Local).AddTicks(9077),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(5825));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 150, DateTimeKind.Local).AddTicks(8075),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(8696));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 22, 22, 54, 150, DateTimeKind.Local).AddTicks(6816),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(7727));

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "FinancialPlan",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
