﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class FixBugAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinancialPlan_PlanManagement_PlanManagementId",
                table: "FinancialPlan");

            migrationBuilder.DropIndex(
                name: "IX_FinancialPlan_PlanManagementId",
                table: "FinancialPlan");

            migrationBuilder.DropColumn(
                name: "PlanManagementId",
                table: "FinancialPlan");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 110, DateTimeKind.Local).AddTicks(3221),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(8338));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 110, DateTimeKind.Local).AddTicks(2764),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(7942));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 107, DateTimeKind.Local).AddTicks(8396),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(8705));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 107, DateTimeKind.Local).AddTicks(6849),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(7573));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(8338),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 110, DateTimeKind.Local).AddTicks(3221));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 737, DateTimeKind.Local).AddTicks(7942),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 110, DateTimeKind.Local).AddTicks(2764));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(8705),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 107, DateTimeKind.Local).AddTicks(8396));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 11, 20, 39, 11, 735, DateTimeKind.Local).AddTicks(7573),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 14, 10, 14, 45, 107, DateTimeKind.Local).AddTicks(6849));

            migrationBuilder.AddColumn<int>(
                name: "PlanManagementId",
                table: "FinancialPlan",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_FinancialPlan_PlanManagementId",
                table: "FinancialPlan",
                column: "PlanManagementId");

            migrationBuilder.AddForeignKey(
                name: "FK_FinancialPlan_PlanManagement_PlanManagementId",
                table: "FinancialPlan",
                column: "PlanManagementId",
                principalTable: "PlanManagement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
