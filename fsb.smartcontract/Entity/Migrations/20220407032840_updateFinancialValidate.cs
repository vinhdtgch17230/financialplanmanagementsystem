﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class updateFinancialValidate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 485, DateTimeKind.Local).AddTicks(4619),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(6271));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 485, DateTimeKind.Local).AddTicks(4049),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(5825));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 480, DateTimeKind.Local).AddTicks(9807),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(8696));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 480, DateTimeKind.Local).AddTicks(8384),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(7727));

            migrationBuilder.CreateTable(
                name: "FinancialPlanValidate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ItemName = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Units = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Currency = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    RealValue = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    ConversionValue = table.Column<decimal>(type: "decimal(65,30)", nullable: false),
                    ExpenditureType = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Error = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PlanManagementId = table.Column<int>(type: "int", nullable: false),
                    FinancialPlanId = table.Column<int>(type: "int", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    UpdatedBy = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    IsDelete = table.Column<bool>(type: "tinyint(1)", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinancialPlanValidate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinancialPlanValidate_FinancialPlan_FinancialPlanId",
                        column: x => x.FinancialPlanId,
                        principalTable: "FinancialPlan",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FinancialPlanValidate_PlanManagement_PlanManagementId",
                        column: x => x.PlanManagementId,
                        principalTable: "PlanManagement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_FinancialPlanValidate_FinancialPlanId",
                table: "FinancialPlanValidate",
                column: "FinancialPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_FinancialPlanValidate_PlanManagementId",
                table: "FinancialPlanValidate",
                column: "PlanManagementId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FinancialPlanValidate");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(6271),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 485, DateTimeKind.Local).AddTicks(4619));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "PlanManagement",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 72, DateTimeKind.Local).AddTicks(5825),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 485, DateTimeKind.Local).AddTicks(4049));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(8696),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 480, DateTimeKind.Local).AddTicks(9807));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedTime",
                table: "LeaveRequest",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(2022, 4, 5, 23, 2, 43, 70, DateTimeKind.Local).AddTicks(7727),
                oldClrType: typeof(DateTime),
                oldType: "datetime(6)",
                oldDefaultValue: new DateTime(2022, 4, 7, 10, 28, 39, 480, DateTimeKind.Local).AddTicks(8384));
        }
    }
}
