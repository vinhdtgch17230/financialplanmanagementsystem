﻿using Entity.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Config
{
    public class FinancialPlanValidateConfig : IEntityTypeConfiguration<FinancialPlanValidate>
    {
        public void Configure(EntityTypeBuilder<FinancialPlanValidate> builder)
        {
            builder.ToTable("FinancialPlanValidate");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd(); //ValueGeneratedOnAdd tự tăng Id
            builder.Property(x => x.IsDelete).HasDefaultValue(false);
        }
    }
}
