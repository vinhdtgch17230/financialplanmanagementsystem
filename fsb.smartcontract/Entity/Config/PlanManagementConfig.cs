﻿using Entity.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Config
{
    public class PlanManagementConfig : IEntityTypeConfiguration<PlanManagement>
    {
        public void Configure(EntityTypeBuilder<PlanManagement> builder)
        {
            builder.ToTable("PlanManagement");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd(); //ValueGeneratedOnAdd tự tăng Id
            builder.Property(x => x.IsDelete).HasDefaultValue(false);
            builder.Property(x => x.CreatedTime).HasDefaultValue(DateTime.Now);
            builder.Property(x => x.UpdatedTime).HasDefaultValue(DateTime.Now);
        }
    }
}