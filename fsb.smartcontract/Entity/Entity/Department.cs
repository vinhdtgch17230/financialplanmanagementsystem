﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entity
{
    public class Department : EntityBase //kế thừa chưas tham số dùng chung
    {
        public string DepartmentName { get; set; }
    }
}
