﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entity
{
    public class PlanManagement :EntityBase
    {
        public string EmployeeName { get; set; }
        public string Title { get; set; }
        public DateTime SubmittedDay { get; set; }
        public int Status { get; set; }
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }
    }
}
