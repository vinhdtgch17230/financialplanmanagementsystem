﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entity
{
    public class FinancialPlan :EntityBase
    {
        public string ItemName { get; set; }
        public string Units { get; set; }
        public string Currency { get; set; }
        public decimal RealValue { get; set; }
        public decimal ConversionValue { get; set; }
        public string ExpenditureType { get; set; }
        //public int PlanManagementId { get; set; }
        //[ForeignKey("PlanManagementId")]
        //public virtual PlanManagement PlanManagement { get; set; }
    }
}
