﻿using Entity.Config;
using Entity.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Context
{
    public class FsbSmartContractContext : IdentityDbContext<User, Role, Guid>
    {
        public FsbSmartContractContext(DbContextOptions<FsbSmartContractContext> ops) : base(ops)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("UserLogin").HasKey(x => new { x.UserId, x.ProviderKey });
            modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("UserRole").HasKey(x => new { x.UserId, x.RoleId });
            modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("UserToken").HasKey(x => new { x.UserId, x.LoginProvider });
            //modelBuilder.Seed();
            modelBuilder.ApplyConfiguration(new CompanyConfig());
            modelBuilder.ApplyConfiguration(new DepartmentConfig());
            modelBuilder.ApplyConfiguration(new ClassConfig());
            modelBuilder.ApplyConfiguration(new LeaveRequestConfig());
            modelBuilder.ApplyConfiguration(new FinancialPlanConfig());
            modelBuilder.ApplyConfiguration(new PlanManagementConfig());
            modelBuilder.ApplyConfiguration(new FinancialPlanValidateConfig());
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Class> Class { get; set; }
        public DbSet<LeaveRequest> LeaveRequests { get; set; }
        public DbSet<FinancialPlan> FinancialPlans { get; set; }
        public DbSet<PlanManagement> PlanManagements { get; set; }
        public DbSet<FinancialPlanValidate> FinancialPlanValidates { get; set; }
    }
}
