﻿using Common;
using Entity.Context;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.FinancialPlan;
using Model.PlanManagement;
using Model.Requests;
using Model.Response;
using OfficeOpenXml;
using Repository.FinancialPlan;
using Repository.PlanManagement;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace fsb.smartcontractapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanManagementController : BaseController
    {
        private readonly IPlanManagementRepository _planManagementRepository;
        private FsbSmartContractContext _context;
        private readonly IFinancialPlanRepository _financialPlanRepository;

        public PlanManagementController(
            IPlanManagementRepository planManagementRepository,
            ILogger<PlanManagementController> logger,
            IConfiguration configuration
            ) : base(configuration, logger)
        {
            _planManagementRepository = planManagementRepository;
        }

        [Route("GetListLeaveRequest")]
        [HttpPost]
        public TableResponse<PlanManagementViewModel> GetListPlanManagement(SearchPlanManagementModel search)
        {
            return _planManagementRepository.GetListPlanManagement(search);
        }

        [Route("CreatePlanManagement")]
        [HttpPost]
        public Response<string> CreatePlanManagement(PlanManagementModel model)
        {
            return _planManagementRepository.CreatePlanManagement(model);
        }

        [Route("GetLeaveRequestById")]
        [HttpPost]
        //[CustomLeaveRequest(LeaveRequestConstants.ADMIN, LeaveRequestConstants.ACCOUNTANT_HO, LeaveRequestConstants.ACCOUNTANT_BP)]
        // là attribute: check quyền đến từng api để thao tác CRUD
        public Response<PlanManagementModel> GetPlanManagementById(PlanManagementModel model)
        {
            return _planManagementRepository.GetPlanManagementById(model);
        }

        [HttpPost]
        [Route("ImportExcelPlanManagement")]
        //[ProducesResponseType(typeof(int), 200)]
        public async Task<List<FinancialPlanModel>> ImportExcelFinancialPlanAsync(IFormFile formFile)
        {

            List<FinancialPlanModel> list = new List<FinancialPlanModel>();// đọc data từ model

            using (var stream = new MemoryStream())//lưu trữ tạm các loại file
            {
                await formFile.CopyToAsync(stream);//nén formFile vào stream thông qua memorystream(bộ nhớ tạm)
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;//đếm số lượng dòng
                    int startRow = 2;

                    for (int i = startRow; i <= rowCount; i++) // đếm và check từng dòng trong file excel
                    {
                        list.Add(new FinancialPlanModel
                        {
                            //PlanManagementId = Convert.ToInt16(worksheet.Cells[i, 1].Value.ToString().Trim()),
                            ItemName = worksheet.Cells[i, 2].Value.ToString().Trim(),
                            Units = worksheet.Cells[i, 3].Value.ToString().Trim(),
                            Currency = worksheet.Cells[i, 4].Value.ToString().Trim(),
                            RealValue = Convert.ToDecimal(worksheet.Cells[i, 5].Value.ToString().Trim()),
                            ConversionValue = Convert.ToDecimal(worksheet.Cells[i, 6].Value.ToString().Trim()),
                            ExpenditureType = worksheet.Cells[i, 7].Value.ToString().Trim()
                            //_context.FinancialPlans.Add(FinancialPlanModel);
                            //_context.SaveChanges();
                        });
                        Entity.Entity.FinancialPlan financialPlan = new Entity.Entity.FinancialPlan();
                        _financialPlanRepository.CreateFinancialPlan(list);
                        _context.FinancialPlans.Add(financialPlan);
                        _context.SaveChanges();
                    }

                }
            }
            //    return await UnitOfWork.SaveChangesAsync() > 0;
            //    _context.Companies.Add(company);
            //    _context.SaveChanges();
            //_context.FinancialPlans.Add(FinancialPlanModel);
            //_context.SaveChanges();
            //return đến repo và cần savechange
            return list;
        }

        //[Route("ImportExcelFinancialPlanAsync")]
        //[HttpPost]
        ////[CustomRole(RoleConstants.ADMIN, RoleConstants.ACCOUNTANT_HO, RoleConstants.ACCOUNTANT_BP)]
        //public async Task<Response<string>> ImportExcelFinancialPlanAsync(IFormFile fileImport)
        //{
        //    Response<string> res = new Response<string>();
        //    try
        //    {
        //        //var financialPlan = HttpContext.GetObjFromJson<FinancialPlanModel>("json");
        //        MemoryStream stream = new MemoryStream();
        //        await fileImport.CopyToAsync(stream);
        //        var bytes = stream.ToArray();
        //        var rs = ReadExcel(bytes);
        //        if (rs.Code == StatusCodes.Status200OK)
        //        {
        //            res = _financialPlanRepository.ImportExcelFinancialPlanAsync(rs.Data);
        //        }
        //        else
        //        {
        //            res.Code = rs.Code;
        //            res.Message = rs.Message;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "");
        //        res.Code = StatusCodes.Status500InternalServerError;
        //        res.Message = "Xảy ra lỗi khi đọc file excel!";
        //    }
        //    return res;
        //}

        //private Response<List<FinancialPlanModel>> ReadExcel(byte[] content)
        //{
        //    Response<List<FinancialPlanModel>> res = new Response<List<FinancialPlanModel>>();
        //    try
        //    {
        //        MemoryStream stream = new MemoryStream(content);
        //        using (var reader = ExcelReaderFactory.CreateReader(stream))
        //        {
        //            var result = reader.AsDataSet();
        //            var table = result.Tables[0];
        //            int startRow = 2;
        //            int maxRow = 1048576 - startRow;
        //            List<FinancialPlanModel> list = new List<FinancialPlanModel>();
        //            for (int i = startRow; i < table.Rows.Count; i++)
        //            {
        //                var PlanManagementId = table.Rows[i][1].ToString();
        //                var ItemName = table.Rows[i][2].ToString();
        //                var Units = table.Rows[i][3].ToString();
        //                var Currency = table.Rows[i][4].ToString();
        //                var RealValue = table.Rows[i][5].ToString();
        //                var ConversionValue = table.Rows[i][6].ToString();
        //                var ExpenditureType = table.Rows[i][7].ToString();


        //                var item = new FinancialPlanModel();
        //                item.PlanManagementId = Convert.ToInt16(PlanManagementId);
        //                item.ItemName = ItemName;
        //                item.Units = Units;
        //                item.Currency = Currency;
        //                item.RealValue = Convert.ToDecimal(RealValue);
        //                item.ConversionValue = Convert.ToDecimal(ConversionValue);
        //                item.ExpenditureType = ExpenditureType;
        //                list.Add(item);

        //            }
        //            res.Data = list;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex, "");
        //        res.Code = StatusCodes.Status500InternalServerError;
        //        res.Message = "Xảy ra lỗi khi đọc file!";
        //    }
        //    return res;
        //}

    }
}


