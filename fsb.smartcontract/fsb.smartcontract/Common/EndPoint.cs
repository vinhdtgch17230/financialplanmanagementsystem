﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontract.Common
{
    public class Path
    {
        //Account
        public static string ACCOUNT_LOGIN = "api/Account/Login";
        public static string ACCOUNT_CREATE = "api/account/create";
        public static string ACCOUNT_UPDATE = "api/account/update";
        public static string ACCOUNT_DELETE = "api/account/delete";
        public static string ACCOUNT_TABLE = "api/account/userTable";
        public static string AUTHENCATE_LOGIN = "api/Account/Authencate";
        public static string AUTHENCATEAZUREAD_LOGIN = "api/Account/AuthencateAzureAD";
        public static string CONFIG = "api/Account/getConfig";

        //UserInfo
        public static string USER_DATATABLE = "api/UserInfo/GetListUser";
        public static string USER_CREATE = "api/UserInfo/CreateUser";
        public static string USER_BYID = "api/UserInfo/GetUserById";
        public static string USER_UPDATE = "api/UserInfo/UpdateUser";
        public static string USER_DELETE = "api/UserInfo/DeleteUser";
        public static string USER_BYUSERNAME = "api/UserInfo/GetUserByUserName";
        public static string USER_CHANGEPASSWORD = "api/UserInfo/SaveChangePassword";
        public static string USERORG_BYID = "api/UserInfo/GetUserOrganizationById";
        public static string USER_FORCOMBO = "api/UserInfo/GetListUserForCombo";
        public static string USER_GETALL = "api/UserInfo/GetAllUser";
        public static string USER_EXPORT = "api/UserInfo/GetDataExportUserInfo";

        //Role
        public static string ROLE_FORCOMBO = "api/Role/GetListRoleForCombo";

        //Company
        public static string COMPANY_DATATABLE = "api/Company/GetListCompany";
        public static string COMPANY_BYID = "api/Company/GetCompanyById";
        public static string COMPANY_CREATE = "api/Company/CreateCompany";
        public static string COMPANY_UPDATE = "api/Company/UpdateCompany";
        public static string COMPANY_DELETE = "api/Company/DeleteCompany";
        public static string COMPANY_FORCOMBO = "api/Company/GetListCompanyForCombo";

        //Department
        public static string DEPARTMENT_FORCOMBO = "api/Department/GetListDepartmentForCombo";
        public static string DEPARTMENT_BYCOMPANYCODE = "api/Department/GetDepartmentByCompanyCode";
        public static string DEPARTMENT_FORUSERID = "api/Department/GetDepartmentByUserId";
        public static string DEPARTMENT_DATATABLE = "api/Department/GetListDepartment";
        public static string DEPARTMENT_BYID = "api/Department/GetDepartmentById";
        public static string DEPARTMENT_CREATE = "api/Department/CreateDepartment";
        public static string DEPARTMENT_UPDATE = "api/Department/UpdateDepartment";
        public static string DEPARTMENT_DELETE = "api/Department/DeleteDepartment";
        public static string DEPARTMENT_ACTIVATE = "api/Department/ActivateDepartment";
        public static string DEPARTMENT_DEACTIVATE = "api/Department/DeactivateDepartment";

        //Role
        public static string ROLE_DATATABLE = "api/Role/GetListRole";
        public static string ROLE_CREATE = "api/Role/CreateRole";
        public static string ROLE_DELETE = "api/Role/DeleteRole";
        public static string ROLE_BYID = "api/Role/GetRoleById";
        public static string ROLE_UPDATE = "api/Role/UpdateRole";

        //LeaveRequest
        public static string LEAVEREQUEST_DATATABLE = "api/LeaveRequest/GetListLeaveRequest";
        public static string CLASS_FORCOMBO = "api/Class/GetListClassForCombo";
        public static string LEAVEREQUEST_CREATE = "api/LeaveRequest/CreateLeaveRequest";
        public static string LEAVEREQUEST_BYID = "api/LeaveRequest/GetLeaveRequestById";
        public static string LEAVEREQUEST_UPDATESTATUS = "api/LeaveRequest/UpdateStatusLeaveRequest";
        public static string LEAVEREQUEST_UPDATE = "api/LeaveRequest/UpdateLeaveRequest";
        public static string LEAVEREQUEST_DELETE = "api/LeaveRequest/DeleteLeaveRequest";

        //PlanManagement
        public static string PLANMANAGEMENT_DATATABLE = "api/PlanManagement/GetListPlanManagement";
        public static string PLANMANAGEMENT_CREATE = "api/PlanManagement/CreatePlanManagement";
        public static string PLANMANAGEMENT_BYID = "api/PlanManagement/GetPlanManagementById";
        public static string PLANMANAGEMENT_IMPORTEXCEL = "api/PlanManagement/ImportExcelFinancialPlanAsync";
    }
}
