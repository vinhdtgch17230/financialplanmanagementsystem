﻿using Model.Department;
using Model.Requests;
using Model.Response;

namespace fsb.smartcontract.Services.Department
{
    public interface IDepartmentService
    {
        TableResponse<DepartmentViewModel> GetListDepartment(SearchDepartmentModel search);
        Response<string> CreateDepartment(DepartmentModel model);
        Response<string> UpdateDepartment(DepartmentModel model);
        Response<DepartmentModel> GetDepartmentById(DepartmentModel model);
        Response<string> DeleteCompany(DepartmentModel model);


    }
}
