﻿using fsb.smartcontract.Services.LeaveRequest;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.LeaveRequest;
using Model.Requests;
using Model.Response;
using NToastNotify;
using System.Net.Http;

namespace fsb.smartcontract.Controllers
{
    public class LeaveRequestController : BaseController
    {
        private readonly ILogger<LeaveRequestController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILeaveRequestService _leaveRequestService;

        public LeaveRequestController(
            ILogger<LeaveRequestController> logger,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IHttpContextAccessor httpContextAccessor,
            ILeaveRequestService leaveRequestServiceService
            ) : base(factory)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _httpContextAccessor = httpContextAccessor;
            _leaveRequestService = leaveRequestServiceService;
        }

        [Route("danh-sach-nghi-phep")]
        public IActionResult LeaveRequests()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetListLeaveRequest(string searchValue)
        {
            SearchLeaveRequestModel searchModel = talbeSearchModel.Get<SearchLeaveRequestModel>();
            searchModel.SearchValue = searchValue;
            var data = _leaveRequestService.GetListLeaveRequest(searchModel);
            if (data.Code != StatusCodes.Status200OK)
                _toastNotification.AddErrorToastMessage(data.Message);
            return Json(data);
        }

        [Route("tao-moi-nghi-phep")]
        public IActionResult CreateLeaveRequest()
        {
            ViewBag.GetListClassForCombo = _leaveRequestService.GetListClassForCombo();
            return View();
        }

        [HttpPost]
        public IActionResult InsertLeaveRequest(LeaveRequestModel model)
        {
            var res = _leaveRequestService.CreateLeaveRequest(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [Route("chi-tiet-nghi-phep/{id}")]
        public IActionResult ViewLeaveRequest(int Id)
        {
            ViewBag.GetListClassForCombo = _leaveRequestService.GetListClassForCombo();
            var model = new LeaveRequestModel();
            model.Id = Id;
            var res = _leaveRequestService.GetLeaveRequestById(model);

            if (res == null)
                res = new Response<LeaveRequestModel>();
            if (res.Data == null)
                res.Data = new LeaveRequestModel();

            return View(res.Data);
        }

        [HttpPost]
        public IActionResult UpdateStatusLeaveRequest(LeaveRequestModel model)
        {
            var res = _leaveRequestService.UpdateStatusLeaveRequest(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [Route("sua-nghi-phep/{id}")]
        public IActionResult EditLeaveRequest(int Id)
        {
            ViewBag.GetListClassForCombo = _leaveRequestService.GetListClassForCombo();
            var model = new LeaveRequestModel();
            model.Id = Id;
            var res = _leaveRequestService.GetLeaveRequestById(model);

            if (res == null)
                res = new Response<LeaveRequestModel>();
            if (res.Data == null)
                res.Data = new LeaveRequestModel();

            return View(res.Data);
        }

        [HttpPost]
        public IActionResult UpdateLeaveRequest(LeaveRequestModel model)
        {
            var res = _leaveRequestService.UpdateLeaveRequest(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }

        [HttpPost]
        public IActionResult DeleteLeaveRequest(LeaveRequestModel model)
        {
            var res = _leaveRequestService.DeleteLeaveRequest(model);
            if (res.Code == StatusCodes.Status200OK)
            {
                _toastNotification.AddSuccessToastMessage(res.Message);
            }
            else
            {
                _toastNotification.AddErrorToastMessage(res.Message);
            }
            return Json(res);
        }
    }
}
